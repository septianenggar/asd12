/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas2;
import java.util.Scanner;
/**
 *
 * @author septianenggars
 */
public class SDLLMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Scanner iBuku = new Scanner(System.in);
        Stack st = new Stack();
        
        int pil=0;
        while(pil != 5){
            System.out.println("**********************");
            System.out.println("Data Buku Perpustakaan");
            System.out.println("**********************");
            System.out.println("\n 1. Entry Judul Buku\n 2. Ambil Buku Teratas\n 3. Cek Judul Buku Teratas\n 4. Info Semua Judul Buku\n 5. Keluar\n");
            System.out.println("**********************");
            pil = Integer.parseInt(sc.nextLine());
            switch(pil){
                case 1:
                    System.out.println("-------------------");
                    System.out.println("Masukkan Judul Buku");
                    System.out.println("-------------------");
                    String jB = iBuku.nextLine();
                    st.push(jB);
                    break;
                case 2:
                    System.out.println("----------------------------------------");
                    System.out.println("Buku pada tumpukan teratas telah diambil");
                    System.out.println("----------------------------------------");
                    st.pop();
                    break;
                case 3:
                    System.out.println("----------------");
                    System.out.println("Cek Buku Teratas");
                    System.out.println("----------------");
                    st.top();
                    break;
                case 4:
                    System.out.println("------------------------");
                    System.out.println("Cetak Seluruh Judul Buku");
                    System.out.println("------------------------");
                    st.print();
                    break;
                case 5:
                    System.out.println("Program dihentikan");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan Tidak Sesuai");
            }
        }
        
    }
}
