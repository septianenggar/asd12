/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas2;

/**
 *
 * @author septianenggars
 */
public class Stack {
    Node head;
    Node tail;
    int size;
    
    public Stack(){
        head = null;
        tail = null;
        size = 0;
    }
    
    public boolean isEmpty(){
        return head == null;
    }
    
    public void push(String data){
        tail = head;
        head = new Node(null, data, null);
        head.data = data;
        head.next = tail;
        head.prev = null;
        
        if(tail != null){
            tail.prev = head;
        }
        size++;
    }
    
    public void pop(){
        if(!isEmpty()){
            head = head.next;
            size--;
        } else{
            System.out.println("Double Linked kosong!");
        }
    }
    
    public void top(){
        
        System.out.println(head.getData());
        
    }
    
    public void print(){
        Node tmp = head;
        while (tmp != null){
            System.out.println(tmp.getData()+" ");
            tmp = tmp.getNext();
        }
    }
}
