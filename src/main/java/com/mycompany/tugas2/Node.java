/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas2;

/**
 *
 * @author septianenggars
 */
public class Node {
    String data;
    Node prev, next;
    
    Node(Node prev, String data, Node next){
        this.prev = prev;
        this.data = data;
        this.next = next;
    }
    
    public String getData(){

        return data;
    }

    public void setData(String data){

        this.data=data;
    }

    public Node getNext(){

        return next;
    }

    public void setNext(Node next){
        this.next=next;
    }

    public Node getPrev(){

        return prev;
    }

    public void setPrev(Node prev){
         this.prev=prev;
    }   
    
    
    
}
