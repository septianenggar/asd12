/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas1;


import java.util.Scanner;
/**
 *
 * @author septianenggars
 */
public class DLLMain {
    public static void main(String[] args) throws Exception {
        DoubleLinkedList d = new DoubleLinkedList();
        Scanner input = new Scanner(System.in);
        Scanner iTData = new Scanner(System.in);
        Scanner iRData = new Scanner(System.in);
        Scanner iCData = new Scanner(System.in);
        Scanner iIndex = new Scanner(System.in);
        
        
        int pil = 0;
        while(pil != 10){
            System.out.println("====================================================");
            System.out.println("PROGRAM PENGOLAHAN ANGKA DENGAN DOUBLY LINKED LIST");
            System.out.println("====================================================");
            System.out.println(" 1. Tambah head\n 2. Tambah tail\n 3. Tambah Data\n 4. Hapus Data Pertama\n 5. Hapus Data Terakhir\n 6. Hapus Data Tertentu\n 7. Cetak\n 8. Cari\n 9. Urut Data\n 10. Keluar");
            System.out.println("===============================");
            pil = Integer.parseInt(input.nextLine());
            switch(pil){
                case 1:
                    System.out.println("Masukkan Data Posisi Head");
                    d.addFirst(Integer.parseInt(input.nextLine()));
                    break;
                case 2:
                    System.out.println("Masukkan Data Posisi Tail");
                    d.addLast(Integer.parseInt(input.nextLine()));
                    break;
                case 3:
                    System.out.println("Masukkan Data");
                    System.out.println("Data node: ");
                    int dData = iTData.nextInt();
                    System.out.println("Alamat pointer:");
                    int dIndex = iIndex.nextInt();
                    d.add(dData, dIndex);
                    break;
                case 4:
                    d.removeFirst();
                    break;
                case 5:
                    d.removeLast();
                    break;
                case 6:
                    System.out.println("Penghapusan data sesuai index");
                    System.out.println("Index: ");
                    int dRIndex = iRData.nextInt();
                    d.remove(dRIndex);
                    break;
                case 7:
                    System.out.println("Cetak Data");
                    d.print();
                    break;
                case 8:
                    System.out.println("Cari Data");
                    System.out.println("Masukkan nilai yang dicari");
                    int cari = iCData.nextInt();
                    d.cariNode(cari);
                    break;
                case 9:
                    System.out.println("Hasil Pengurutan");
                    d.bubbleSort();
                    break;
                case 10:
                    System.out.println("Program dihentikan");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan Tidak Sesuai");
            }
        }
    }
}
