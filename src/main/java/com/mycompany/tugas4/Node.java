/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas4;

/**
 *
 * @author septianenggars
 */
public class Node {
    Mahasiswa data;
    Node prev;
    Node next;
    
    Node(Node prev, Mahasiswa data, Node next){
        this.prev = prev;
        this.data = data;
        this.next = next;
    }
}
