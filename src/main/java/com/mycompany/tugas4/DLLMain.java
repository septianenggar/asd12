/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas4;


import java.util.Scanner;
/**
 *
 * @author septianenggars
 */
public class DLLMain {
    public static void main(String[] args) throws Exception {
        DoubleLinkedList d = new DoubleLinkedList();
        Mahasiswa m;
        Scanner input = new Scanner(System.in);
        Scanner iTNIM = new Scanner(System.in);
        Scanner iTNama = new Scanner(System.in);
        Scanner iTIPK = new Scanner(System.in);
        Scanner iRData = new Scanner(System.in);
        Scanner iCData = new Scanner(System.in);
        Scanner iIndex = new Scanner(System.in);
        
        
        int pil = 0;
        while(pil != 10){
            System.out.println("=================================");
            System.out.println("SISTEM PENGOLAHAN DATA MAHASISWA");
            System.out.println("=================================");
            System.out.println(" 1. Tambah Data Awal\n 2. Tambah Data Akhir\n 3. Tambah Data Index Tertentu\n 4. Hapus Data Pertama\n 5. Hapus Data Terakhir\n 6. Hapus Data Tertentu\n 7. Cetak\n 8. Cari NIM\n 9. Urut Data IPK-DESC\n 10. Keluar");
            System.out.println("=================================");
            pil = Integer.parseInt(input.nextLine());
            switch(pil){
                case 1:
                    System.out.println("Masukkan Data Mahasiswa Posisi Awal");
                    System.out.println("NIM: ");
                    int niH = iTNIM.nextInt();
                    System.out.println("Nama: ");
                    String naH = iTNama.nextLine();
                    System.out.println("Ipk: ");
                    float ipH = iTIPK.nextFloat();
                    
                    Mahasiswa mhsH = new Mahasiswa(niH, naH, ipH);
                    d.addFirst(mhsH);
                    //d.addFirst(Integer.parseInt(input.nextLine()));
                    break;
                case 2:
                    System.out.println("Masukkan Data Posisi Akhir");
                    System.out.println("NIM: ");
                    int niL = iTNIM.nextInt();
                    System.out.println("Nama: ");
                    String naL = iTNama.nextLine();
                    System.out.println("Ipk: ");
                    float ipL = iTIPK.nextFloat();
                    
                    Mahasiswa mhsL = new Mahasiswa(niL, naL, ipL);
                    d.addLast(mhsL);
                    //d.addLast(Integer.parseInt(input.nextLine()));
                    break;
                case 3:
                    System.out.println("Masukkan Data Mahasiswa");
                    System.out.println("Data node: ");
                    System.out.println("NIM: ");
                    int niA = iTNIM.nextInt();
                    System.out.println("Nama: ");
                    String naA = iTNama.nextLine();
                    System.out.println("Ipk: ");
                    float ipA = iTIPK.nextFloat();
                    
                    Mahasiswa mhsA = new Mahasiswa(niA, naA, ipA);
                    
                    System.out.println("Data Mahasiswa ini akan dimasukkan di urutan ke-");
                    int dIndex = iIndex.nextInt();
                    d.add(mhsA, dIndex);
                    break;
                case 4:
                    d.removeFirst();
                    break;
                case 5:
                    d.removeLast();
                    break;
                case 6:
                    System.out.println("Penghapusan data sesuai index");
                    System.out.println("Index: ");
                    int dRIndex = iRData.nextInt();
                    d.remove(dRIndex);
                    break;
                case 7:
                    System.out.println("Cetak Data");
                    d.print();
                    break;
                case 8:
                    System.out.println("Cari Data");
                    System.out.println("Masukkan NIM yang dicari");
                    int cari = iCData.nextInt();
                    d.cariNIM(cari);
                    break;
                case 9:
                    System.out.println("Hasil Pengurutan IPK (Desc)");
                    d.urutIPK();
                    break;
                case 10:
                    System.out.println("Program dihentikan");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan Tidak Sesuai");
            }
        }
    }
}
