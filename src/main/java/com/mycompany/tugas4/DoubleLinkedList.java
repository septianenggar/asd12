/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas4;


/**
 *
 * @author septianenggars
 */
public class DoubleLinkedList {
    Node head;
    int size;
    
    
    public DoubleLinkedList(){
        head = null;
        size = 0;
    }
    
    public boolean isEmpty(){
        return head == null;
    }
    
    public void addFirst(Mahasiswa item){
       // item = mhs;
        if(isEmpty()){
            
            head = new Node(null, item, null);
            
        } else{
            Node newNode = new Node(null, item,head);
            head.prev = newNode;
            head = newNode;
        }
        size++;
    }
    
    public void addLast(Mahasiswa item){
       // item = mhs;
        if(isEmpty()){
            addFirst(item);
        } else{
            Node current = head;
            while(current.next != null){
                current = current.next;
            }
            Node newNode = new Node(current, item, null);
            current.next = newNode;
            size++;
        }
    }
    
    public void add(Mahasiswa item, int index) throws Exception{
       // item = mhs;
        if(isEmpty()){
            addFirst(item);
        } else if(index <0 || index > size){
            throw new Exception("Nilai indeks di luar batas");
        } else{
            Node current = head;
            int i = 0;
            while (i< index){
                current = current.next;
                i++;
            }
            if(current.prev == null){
                Node newNode = new Node(null, item, current);
                current.prev = newNode;
                head = newNode;
            } else {
                Node newNode = new Node(current.prev, item, current);
                newNode.prev = current.prev;
                newNode.next = current;
                current.prev.next = newNode;
                current.prev = newNode;
            }
        }
        size++;
    }
    
    public int size(){
        return size;
    }
    
    public void clear(){
        head = null;
        size = 0;
    }
    
    public void print(){
        if(!isEmpty()){
            Node tmp = head;
            while(tmp != null){
                System.out.println("NIM: "+tmp.data.nim + "\n Nama: " +tmp.data.nama+ "\n ipk: "+tmp.data.ipk);
                tmp = tmp.next;
            }
            System.out.println("\nsemua data berhasil dicetak");
        } else{
            System.out.println("Linked List Kosng");
        }
    }
    
    public void removeFirst() throws Exception{
        if(isEmpty()){
            throw new Exception("Linked List masih kosong");
        } else if(size == 1){
            removeLast();
        } else {
            head = head.next;
            head.prev = null;
            size--;
        }
    }
    
    public void removeLast() throws Exception{
        if(isEmpty()){
            throw new Exception("Linked List masih kosong");
            
        } else if(head.next == null){
            head = null;
            size--;
            return;
        }
        Node current = head;
        while(current.next.next != null){
            current = current.next;
        }
        current.next = null;
        size--;
    }
    
    public void remove(int index) throws Exception{
        if(isEmpty() || index >= size){
            throw new Exception("nilai di luar batas");
        } else if (index == 0){
            removeFirst();
        } else{
            Node current = head;
            int i = 0;
            while(i < index){
                current = current.next; i++;
            }
            if(current.next == null){
                current.prev.next = null;
            } else if(current.prev == null){
                current = current.next;
                current.prev = null;
                head = current;
            } else{
                current.prev.next = current.next;
                current.next.prev = current.prev;
            }
            size--;
        }
    }
    
    public void cariNIM(int value){
        int posisi = 1;
        boolean flag = false;
        Node current = head;
        
        if(head == null){
            System.out.println("Linked List kosong");
            return;
        }
        while( current != null){
            if(current.data.nim == value){
                flag = true;
                break;
            }
            current = current.next; posisi++;
        }
        if(flag){
            System.out.println("Data " + value + " berada di node ke- " + posisi);
            System.out.println("IDENTITAS: \n NIM: "+current.data.nim+"\n Nama: "+current.data.nama+"\n IPK: "+current.data.ipk);
        } else{
            System.out.println("Data yang dicari tidak ditemukan");
        }
        
    }
    
    public void urutIPK(){
        Node current = null;
        Node index = null;
        int tempNIM;
        String tempNama;
        float tempIpk;
        
        if(head == null){
            System.out.println("Linked List Kosong");
        }
        else{
            for(current=head; current.next != null; current = current.next){
                for(index = current.next; index != null; index = index.next){
                    if(current.data.ipk > index.data.ipk){
                        tempNIM = current.data.nim;
                        current.data.nim = index.data.nim;
                        index.data.nim = tempNIM;
                        
                        tempNama = current.data.nama;
                        current.data.nama = index.data.nama;
                        index.data.nama = tempNama;
                        
                        tempIpk = current.data.ipk;
                        current.data.ipk = index.data.ipk;
                        index.data.ipk = tempIpk;
                    }
                }
            }
        }
    }
    
    
}
