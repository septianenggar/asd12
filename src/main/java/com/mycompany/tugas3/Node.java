/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas3;

/**
 *
 * @author septianenggars
 */
public class Node {
    PengantriVaksin data;
    Node prev, next;
    
    Node(Node prev, PengantriVaksin data, Node next){
        this.prev = prev;
        this.data = data;
        this.next = next;
    }
    
    public PengantriVaksin getData(){
        return data;
    }
    
    public Node getNext(){

        return next;
    }
    
    public Node getPrev(){

        return prev;
    }
}
