/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas3;
import java.util.Scanner;
/**
 *
 * @author septianenggars
 */
public class QLLMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Scanner iNo = new Scanner(System.in);
        Scanner iNa = new Scanner(System.in);
        Queue q = new Queue();
        
        int pil=0;
        while(pil != 4){
            System.out.println("+++++++++++++++++++++++++++++");
            System.out.println("PENGANTRI VAKSIN EXTRAVAGANZA");
            System.out.println("+++++++++++++++++++++++++++++");
            System.out.println("\n 1. Tambah Data Penerima Vaksin\n 2. Hapus Data Pengantri Vaksin\n 3. Daftar Penerima Vaksin\n 4. Keluar");
            System.out.println("+++++++++++++++++++++++++++++");
            pil = Integer.parseInt(sc.nextLine());
            
            switch(pil){
                case 1:
                    System.out.println("-----------------------------");
                    System.out.println("Masukkan Data Penerima Vaksin");
                    System.out.println("-----------------------------");
                    System.out.println("Nomor Antrian: ");
                    int no = iNo.nextInt();
                    System.out.println("Nama Penerima: ");
                    String na = iNa.nextLine();
                    PengantriVaksin pv = new PengantriVaksin(no,na);
                    q.enqueue(pv);
                    break;
                case 2:
                    q.dequeue();
                    break;
                case 3:
                    q.print();
                    break;
                case 4:
                    System.out.println("Program dihentikan");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan Tidak Sesuai");
            }
        }
    }
}
