/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tugas3;

/**
 *
 * @author septianenggars
 */
public class Queue {
    Node head;
    Node tail;
    int size;
    
    
    public Queue(){
        head = null;
        tail = null;
        size = 0;
    }
    
    public boolean isEmpty(){
        if(size ==0){
            return true;
        } else{
            return false;
        }
    }
    
    public int getSize(){
        return size;
    }
    
    public void enqueue(PengantriVaksin data){
        Node tmp = new Node(null, data, null);
        tmp.data = data;
        tmp.next = null;
        
        if(head == null){
            head=tail=tmp;
            head.prev = null;
        } else{
            tail.next = tmp;
            tmp.prev = tail;
            tail = tmp;
        }
        size++;
    }
    
    public PengantriVaksin dequeue(){
        
        System.out.println(head.data.nama+" telah selesai divaksinasi.");
        Node tmp = head.next;
        
   
        if(isEmpty()){
            System.out.println("Antrian masih kosong");
        } else{
            
            head.next = tmp.next;
            head = tmp;
            size--;
            print();
            
            
        }
        return tmp.data;
        
    }
    
    public void print(){
        System.out.println("++++++++++++++++++++++++");
        System.out.println("Daftar Pengantri Vaksin");
        System.out.println("++++++++++++++++++++++++");
        
        if(isEmpty()){
            System.out.println("Tidak ada yang mengantri vaksin");
        }
        Node tmp = head;
        System.out.println("|No.\t |Nama\t |");
        while(tmp != tail.getNext()){
            
            System.out.println("|"+tmp.data.urutan+"\t |"+tmp.data.nama+"\t |");
            tmp = tmp.getNext();
        }
        System.out.println("Sisa Antrian: "+size);
        
        
    }
}
